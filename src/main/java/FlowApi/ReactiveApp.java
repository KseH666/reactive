package FlowApi;

import lombok.SneakyThrows;

import java.util.concurrent.Flow;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.SubmissionPublisher;

public class ReactiveApp {
    @SneakyThrows
    public static void main(String[] args) {
        final int BUFFER = 2;
        SubmissionPublisher<Integer> submissionPublisher = new SubmissionPublisher<>(ForkJoinPool.commonPool(), BUFFER);
        Flow.Subscriber<Integer> customSubscriber = new Flow.Subscriber<>() {
            public Flow.Subscription subscription;
            private int count;
            private final int request = 2;

            @Override
            public void onSubscribe(Flow.Subscription subscription) {
                System.out.println("Подписка");
                this.subscription = subscription;
                System.out.println("Запрос на контент (" + request + " шт)");
                subscription.request(request);
            }

            @Override
            public void onNext(Integer integer) {
                System.out.println("Поток - " + Thread.currentThread().getName() + " | Получен контент # " + integer);
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                count++;
                if (count % 2 == 0) {
                    System.out.println("Запрос на контент (" + request + " шт)");
                    subscription.request(request);
                }
            }

            @Override
            public void onError(Throwable throwable) {
                System.out.println("Ошибка");
                throwable.printStackTrace();
            }

            @Override
            public void onComplete() {
                System.out.println("Завершено");
            }
        };
        submissionPublisher.subscribe(customSubscriber);
        for (int a = 0; a < 10; a++) {
            System.out.println("Поток - " + Thread.currentThread().getName() + " | Создан контент # " + a);
            submissionPublisher.submit(a);
        }
        submissionPublisher.close();
        Thread.sleep(10000);
    }
}
