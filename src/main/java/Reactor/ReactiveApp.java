package Reactor;

import lombok.SneakyThrows;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;
import reactor.core.publisher.Flux;
import reactor.core.scheduler.Schedulers;

public class ReactiveApp {
    @SneakyThrows
    public static void main(String[] args) {
        Flux.range(1, 10).log().subscribeOn(Schedulers.newParallel("Publisher"))
                .subscribe(new Subscriber<>() {
                    public Subscription subscription;
                    private int count;
                    private final int request = 2;

                    @Override
                    public void onSubscribe(Subscription subscription) {
                        System.out.println("Подписка");
                        this.subscription = subscription;
                        System.out.println("Запрос на контент (" + request + " шт)");
                        subscription.request(request);
                    }

                    @Override
                    public void onNext(Integer integer) {
                        System.out.println("Поток - " + Thread.currentThread().getName() + " | Получен контент # " + integer);
                        try {
                            Thread.sleep(1000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        count++;
                        if (count % 2 == 0) {
                            System.out.println("Запрос на контент (" + request + " шт)");
                            subscription.request(request);
                        }
                    }

                    @Override
                    public void onError(Throwable throwable) {
                        System.out.println("Ошибка");
                        throwable.printStackTrace();
                    }

                    @Override
                    public void onComplete() {
                        System.out.println("Завершено");
                    }
                });
        Thread.sleep(10000);
        System.exit(0);
    }
}
